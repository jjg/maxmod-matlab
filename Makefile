default:

test-matlab :
	matlab -nodisplay -r testsuite

test-octave :
	octave --no-window-system --eval testsuite
